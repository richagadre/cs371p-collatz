// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

/*

1 10 20
100 200 125
201 210 89
900 1000 174


*/

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(123, 456)), make_tuple(123, 456, 144));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(3000, 250000)), make_tuple(3000, 250000, 443));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(764, 897)), make_tuple(764, 897, 179));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(400,4000 )), make_tuple(400, 4000, 238));
}

TEST(CollatzFixture, cycle0) {
    ASSERT_EQ(cycle_length(12), 10);
}

TEST(CollatzFixture, cycle1) {
    ASSERT_EQ(cycle_length(25), 24);
}

TEST(CollatzFixture, cycle2) {
    ASSERT_EQ(cycle_length(350), 82);
}

TEST(CollatzFixture, cycle3) {
    ASSERT_EQ(cycle_length(1000), 112);
}

TEST(CollatzFixture, cycle4) {
    ASSERT_EQ(cycle_length(2756), 129);
}

TEST(CollatzFixture, cycle5) {
    ASSERT_EQ(cycle_length(91), 93);
}

TEST(CollatzFixture, cycle6) {
    ASSERT_EQ(cycle_length(174), 32);
}

TEST(CollatzFixture, cycle7) {
    ASSERT_EQ(cycle_length(56789), 61);
}














// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
