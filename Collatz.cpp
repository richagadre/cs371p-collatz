// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

int cache [1000000] = { 0 };

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

/* helper method to calculate the cycle length of a particular number */
int cycle_length(long i) {
    assert (i > 1);
    int c = 0;
    /* save the original value of the number for caching */
    long temp = i;
    /* following collatz conjection code */
    while(i > 1) {
        if (i % 2 == 0) {
            i /= 2;
            ++c;
        } else {
            i = 3*i + 1;
            i /= 2;
            c += 2;
        }

        /* if a valid index and already has a value, we can just add to that */
        if (i >= 0 && i < 1000000 && cache[i] != 0) {
            c += cache[i];
            break;
        }
    }

    /* cache the new found value */
    i = temp;
    if(i < 1000000) {
        cache[i] = c;
    }

    assert(c > 0);
    return c;

}



tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j, init_max, m,  init_min, current = 0, result = 0;
    tie(i, j) = p;

    /* so we know where to loop from */
    if(i > j) {
        init_max = i;
        init_min = j;
    } else {
        init_max = j;
        init_min = i;
    }

    /* optimization to minimize the number of iterations we have to go through */
    m = (init_max/2) + 1;

    if(m > init_min)
        init_min = m;

    /* initial values */
    cache[0] = 0;
    cache[1] = 1;



    for (long i = init_min; i <=init_max; i++) {
        current = 0;
        /* if already cached, return */
        if(cache[i] != 0) {
            if(i < 1000000) {
                current = cache[i];
            }

            /* otherwise, find the cycle length */
        } else {
            current = cycle_length(i);
        }
        /* update the result (max cycle length as we go) */
        if (current > result)
            result = current;
    }



    return make_tuple(i, j, result);
}


// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
