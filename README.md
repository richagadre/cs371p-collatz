# CS371p: Object-Oriented Programming Collatz Repo

* Name: Richa Gadre

* EID: rkg723

* GitLab ID: richagadre

* HackerRank ID: richagadre

* Git SHA: 7372c0126943f729f66d331c86428fe8161a3759

* GitLab Pipelines: https://gitlab.com/richagadre/cs371p-collatz/-/pipelines

* Estimated completion time: 12.0 hours

* Actual completion time: 16.0 hours

* Comments: took me more time than expected because I was not expecting the segfault issues which took a while to resolve and I had trouble making the acceptance test files which took much more time than expected 
